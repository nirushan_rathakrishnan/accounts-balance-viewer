﻿using AccountsBalanceViewer.Contracts;
using System;
using System.Collections.Generic;
using System.IO;

namespace AccountsBalanceViewer.Services
{
    public class AccountsTextFileReaderService : IAccountsTextFileReaderService
    {
        public Dictionary<string, decimal> ReadFile(Stream stream)
        {
            Dictionary<string, decimal> accounts = new Dictionary<string, decimal>();

            using (StreamReader sr = new StreamReader(stream))
            {
                while (sr.Peek() >= 0)
                {
                    var accountsData = sr.ReadLine().Split('\t');
                    accounts.Add(accountsData[0], Convert.ToDecimal(accountsData[1]));
                }
            }

            return accounts;
        }
    }
}
