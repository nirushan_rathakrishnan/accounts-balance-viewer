﻿using AccountsBalanceViewer.Contracts;
using AccountsBalanceViewer.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AccountsBalanceViewer.Domain.Dto;
using AccountsBalanceViewer.Domain.Entities;

namespace AccountsBalanceViewer.Services
{
    public class AccountsBalanceService : IAccountsBalanceService
    {
        private IDataContext datacontext;

        public AccountsBalanceService(IDataContext datacontext)
        {
            this.datacontext = datacontext;
        }

        public async Task<IEnumerable<AccountsBalanceDto>> GetAccountsBalanceAsync(int month, int year)
        {
            var accountCategoryTypeBalances = await datacontext.AccountCategoryTypes
                .Include(a => a.AccountsBalances)
                .ToListAsync();

            var accountBalances = new List<AccountsBalanceDto>();

            foreach (var accountCategoryBalance in accountCategoryTypeBalances)
            {
                decimal totalBalance = 0;
                foreach (var accountBalance in accountCategoryBalance.AccountsBalances.Where(b => b.Date <= new DateTime(year, month, 1)))
                {
                    totalBalance += accountBalance.Balance;
                }

                accountBalances.Add(new AccountsBalanceDto
                {
                    AccountCategoryType = accountCategoryBalance.Type,
                    Balance = totalBalance
                });

            }

            return accountBalances;
        }

        public async Task<IEnumerable<ReportDataDto>> GetReportDataAsync(int toMonth, int toYear, int fromMonth, int fromYear)
        {
            return await datacontext.AccountCategoryTypes.Select(a => new ReportDataDto
            {
                AccountCategoryType = a.Type,
                data = a.AccountsBalances.Where(b => b.Date >= new DateTime(fromYear, fromMonth, 1) && b.Date <= new DateTime(toYear, toMonth, 1)).Select(b => new AccountCategoryTypeDataDto
                {
                    Balance = b.Balance,
                    Month = b.Date.Month,
                    Year = b.Date.Year
                })
            }).ToListAsync();
        }

        public async Task<bool> UpdateAccountsAsync(Dictionary<string, decimal> accountsData, int month, int year)
        {
            var accountTypes = await datacontext.AccountCategoryTypes.ToListAsync();

            //check weather all accounts type exist
            if (accountTypes.Count > accountsData.Count)
            {
                throw new ArgumentException("File doesn't have all the account types.");
            }

            if (accountTypes.Count < accountsData.Count)
            {
                throw new ArgumentException("File doesn't have valid account types.");
            }

            foreach (var account in accountsData)
            {
                var accountType = accountTypes.FirstOrDefault(a => a.Type.ToLower().Equals(account.Key.Trim().ToLower()));
                if (accountType != null)
                {
                    var existingBalance = await datacontext.AccountsBalances
                        .FirstOrDefaultAsync(a => a.Date.Month == month && a.Date.Year == year && a.AccountCategoryTypeId == accountType.Id);

                    if (existingBalance == null)
                    {
                        datacontext.AccountsBalances.Add(new AccountsBalance
                        {
                            Balance = account.Value,
                            AccountCategoryTypeId = accountType.Id,
                            Date = new DateTime(year, month, 1)
                        });
                    }
                    else
                    {
                        //overwrite balances
                        existingBalance.Balance = account.Value;
                    }
                }
                else
                {
                    throw new ArgumentException($"{account.Key} is not a valid account type.");
                }
            }

            await datacontext.SaveChangesAsync();
            return true;
        }
    }
}
