﻿using AccountsBalanceViewer.Contracts;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AccountsBalanceViewer.Services
{
    public class AccountsExcelFileReaderService : IAccountsExcelFileReaderService
    {
        public Dictionary<string, decimal> ReadFile(Stream stream)
        {
            Dictionary<string, decimal> accounts = new Dictionary<string, decimal>();

            //read stream as excel file
            using (var package = new ExcelPackage(stream))
            {
                var currentSheet = package.Workbook.Worksheets;
                var workSheet = currentSheet.First();
                var noOfCol = workSheet.Dimension.End.Column;
                var noOfRow = workSheet.Dimension.End.Row;

                //iterate through rows
                for (int rowIterator = 1; rowIterator <= noOfRow; rowIterator++)
                {
                    //get excel files col values
                    var accountTypeColValue = workSheet.Cells[rowIterator, 1].Value.ToString();
                    var balanceColType = workSheet.Cells[rowIterator, 2].Value.ToString();

                    accounts.Add(accountTypeColValue, Convert.ToDecimal(balanceColType));
                }
            }

            return accounts;
        }
    }
}
