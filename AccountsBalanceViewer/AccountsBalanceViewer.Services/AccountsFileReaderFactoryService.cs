﻿using AccountsBalanceViewer.Contracts;
using System;

namespace AccountsBalanceViewer.Services
{
    public class AccountsFileReaderFactoryService : IAccountsFileReaderFactoryService
    {
        private IAccountsExcelFileReaderService accountsExcelFileReaderService;
        private IAccountsTextFileReaderService accountsTextFileReaderService;

        public AccountsFileReaderFactoryService(IAccountsTextFileReaderService accountsTextFileReaderService,
            IAccountsExcelFileReaderService accountsExcelFileReaderService)
        {
            this.accountsTextFileReaderService = accountsTextFileReaderService;
            this.accountsExcelFileReaderService = accountsExcelFileReaderService;
        }
        

        public IAccountsFileReaderService GetFileReaderService(string filename)
        {
            IAccountsFileReaderService fileReaderService = null;

            var filenameMetaData = filename.Split('.');
            if (filenameMetaData != null && filenameMetaData.Length > 1)
            {
                //get fileReaderService depending on the file extension
                switch(filenameMetaData[filenameMetaData.Length - 1].Trim().ToLower())
                {
                    case "txt":
                        fileReaderService = accountsTextFileReaderService;
                        break;
                    case "xlsx":
                    case "xls":
                        fileReaderService = accountsExcelFileReaderService;
                        break;
                    default:
                        throw new ArgumentException("File type is not supported.");
                }
            }
            else
            {
                throw new ArgumentException("Invalid file.");
            }

            return fileReaderService;
        }
    }
}
