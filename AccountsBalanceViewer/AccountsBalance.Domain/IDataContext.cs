﻿using AccountsBalanceViewer.Domain.Entities;
using System.Data.Entity;
using System.Threading.Tasks;

namespace AccountsBalanceViewer.Domain
{
    /// <summary>
    /// DataContext interface
    /// </summary>
    public interface IDataContext
    {
        IDbSet<AccountCategoryType> AccountCategoryTypes { get; set; }

        IDbSet<AccountsBalance> AccountsBalances { get; set; }

        Task<int> SaveChangesAsync();
    }
}
