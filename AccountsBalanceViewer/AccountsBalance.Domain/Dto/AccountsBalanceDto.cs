﻿namespace AccountsBalanceViewer.Domain.Dto
{
    public class AccountsBalanceDto
    {
        public string AccountCategoryType { get; set; }

        public decimal Balance { get; set; }
    }
}
