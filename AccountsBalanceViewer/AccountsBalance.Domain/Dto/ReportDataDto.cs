﻿using System.Collections.Generic;

namespace AccountsBalanceViewer.Domain.Dto
{
    public class ReportDataDto
    {
        public string AccountCategoryType { get; set; }

        public IEnumerable<AccountCategoryTypeDataDto> data { get; set; }
    }

    public class AccountCategoryTypeDataDto
    {
        public int Month { get; set; }

        public int Year { get; set; }

        public decimal Balance { get; set; }
    }
}
