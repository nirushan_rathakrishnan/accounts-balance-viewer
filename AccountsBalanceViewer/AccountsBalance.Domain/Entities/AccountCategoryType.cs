﻿using System.Collections.Generic;

namespace AccountsBalanceViewer.Domain.Entities
{
    public class AccountCategoryType
    {
        public int Id { get; set; }

        public string Type { get; set; }
        
        public ICollection<AccountsBalance> AccountsBalances { get; set; }
    }
}
