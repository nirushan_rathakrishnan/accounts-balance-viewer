﻿using System;

namespace AccountsBalanceViewer.Domain.Entities
{
    public class AccountsBalance
    {
        public int Id { get; set; }

        public decimal Balance { get; set; }

        public DateTime Date { get; set; }

        public int AccountCategoryTypeId { get; set; }

        public AccountCategoryType AccountCategoryType { get; set; }
    }
}
