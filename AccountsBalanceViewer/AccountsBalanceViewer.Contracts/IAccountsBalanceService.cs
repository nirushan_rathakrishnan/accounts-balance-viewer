﻿using AccountsBalanceViewer.Domain.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AccountsBalanceViewer.Contracts
{
    public interface IAccountsBalanceService
    {
        /// <summary>
        /// Update accounts for a specified month
        /// </summary>
        /// <param name="accountsData">data</param>
        /// <param name="month">month</param>
        /// <param name="year">year</param>
        /// <returns>success flag</returns>
        Task<bool> UpdateAccountsAsync(Dictionary<string, decimal> accountsData, int month, int year);

        /// <summary>
        /// Get accounts data to show straightforward view
        /// </summary>
        /// <param name="month">month</param>
        /// <param name="year">year</param>
        /// <returns>data</returns>
        Task<IEnumerable<AccountsBalanceDto>> GetAccountsBalanceAsync(int month, int year);

        /// <summary>
        /// Get accounts data to show report
        /// </summary>
        /// <param name="toMonth">upto which month data should be shown</param>
        /// <param name="toYear">upto which year data should be shown</param>
        /// <param name="fromMonth">from which month data should be shown</param>
        /// <param name="fromYear">from which year data should be shown</param>
        /// <returns>data</returns>
        Task<IEnumerable<ReportDataDto>> GetReportDataAsync(int toMonth, int toYear, int fromMonth, int fromYear);
    }
}
