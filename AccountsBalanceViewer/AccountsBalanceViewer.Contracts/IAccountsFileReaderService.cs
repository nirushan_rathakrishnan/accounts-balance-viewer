﻿using System.Collections.Generic;
using System.IO;

namespace AccountsBalanceViewer.Contracts
{
    public interface IAccountsFileReaderService
    {
        /// <summary>
        /// Read file and return data in a dictionary format
        /// </summary>
        /// <param name="stream">file stream</param>
        /// <returns>data</returns>
        Dictionary<string, decimal> ReadFile(Stream stream);
    }
}
