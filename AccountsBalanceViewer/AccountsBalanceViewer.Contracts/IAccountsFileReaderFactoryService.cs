﻿namespace AccountsBalanceViewer.Contracts
{
    public interface IAccountsFileReaderFactoryService
    {
        IAccountsFileReaderService GetFileReaderService(string filename);
    }
}
