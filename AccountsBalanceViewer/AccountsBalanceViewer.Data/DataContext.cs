﻿using AccountsBalanceViewer.Domain;
using AccountsBalanceViewer.Domain.Entities;
using AccountsBalanceViewer.IdentityFramework.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using System.Threading;
using System.Threading.Tasks;

namespace AccountsBalanceViewer.Data
{
    public class DataContext : IdentityDbContext<ApplicationUser>, IDataContext
    {
        public DataContext()
            : base("DbConnection", throwIfV1Schema: false)
        {
        }

        public virtual IDbSet<AccountCategoryType> AccountCategoryTypes { get; set; }

        public virtual IDbSet<AccountsBalance> AccountsBalances { get; set; }

        public static DataContext Create()
        {
            return new DataContext();
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            return base.SaveChangesAsync(cancellationToken);
        }
    }
}
