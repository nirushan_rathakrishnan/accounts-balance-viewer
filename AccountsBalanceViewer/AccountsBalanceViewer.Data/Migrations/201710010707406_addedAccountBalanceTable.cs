namespace AccountsBalanceViewer.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedAccountBalanceTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AccountsBalances",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Balance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Date = c.DateTime(nullable: false),
                        AccountCategoryTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AccountCategoryTypes", t => t.AccountCategoryTypeId, cascadeDelete: true)
                .Index(t => t.AccountCategoryTypeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AccountsBalances", "AccountCategoryTypeId", "dbo.AccountCategoryTypes");
            DropIndex("dbo.AccountsBalances", new[] { "AccountCategoryTypeId" });
            DropTable("dbo.AccountsBalances");
        }
    }
}
