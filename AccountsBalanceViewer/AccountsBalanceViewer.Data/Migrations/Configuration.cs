namespace AccountsBalanceViewer.Data.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<AccountsBalanceViewer.Data.DataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(AccountsBalanceViewer.Data.DataContext context)
        {
            context.AccountCategoryTypes.AddOrUpdate(
                new Domain.Entities.AccountCategoryType { Id = 1, Type = "R&D" },
                new Domain.Entities.AccountCategoryType { Id = 2, Type = "Canteen" },
                new Domain.Entities.AccountCategoryType { Id = 3, Type = "CEO�s car" },
                new Domain.Entities.AccountCategoryType { Id = 4, Type = "Marketing" },
                new Domain.Entities.AccountCategoryType { Id = 5, Type = "Parking fines" }
                );

            if (!context.Roles.Any(r => r.Name.Equals("Admin")))
            {
                context.Roles.AddOrUpdate(
                new Microsoft.AspNet.Identity.EntityFramework.IdentityRole { Name = "Admin" }
                );
            }

            if (!context.Roles.Any(r => r.Name.Equals("User")))
            {
                context.Roles.AddOrUpdate(
                new Microsoft.AspNet.Identity.EntityFramework.IdentityRole { Name = "User" }
                );
            }
        }
    }
}
