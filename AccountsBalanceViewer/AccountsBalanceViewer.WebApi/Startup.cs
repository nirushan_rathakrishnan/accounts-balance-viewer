﻿using Microsoft.Owin;
using Owin;

//configure owin to start
[assembly: OwinStartup(typeof(AccountsBalanceViewer.Startup))]
namespace AccountsBalanceViewer
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            //Note:- for testing purpose, enable request from all urls. In future we can restrict it.
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

            ConfigureAuth(app);
        }
    }
}
