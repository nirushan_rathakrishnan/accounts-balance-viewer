﻿using AccountsBalanceViewer.Domain;
using AccountsBalanceViewer.Data;
using AccountsBalanceViewer.WebApi.DependencyResolvers;
using Microsoft.Owin.Security.OAuth;
using Microsoft.Practices.Unity;
using System.Web.Http;
using AccountsBalanceViewer.Contracts;
using AccountsBalanceViewer.Services;
using Newtonsoft.Json.Serialization;

namespace AccountsBalanceViewer.WebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            //change return object to camel case 
            var json = GlobalConfiguration.Configuration.Formatters.JsonFormatter;
            json.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            //configure unity to have dependency injection
            var container = new UnityContainer();
            container.RegisterType<IDataContext, DataContext>(new HierarchicalLifetimeManager());
            container.RegisterType<IAccountsBalanceService, AccountsBalanceService>(new HierarchicalLifetimeManager());
            container.RegisterType<IAccountsFileReaderFactoryService, AccountsFileReaderFactoryService>(new HierarchicalLifetimeManager());
            container.RegisterType<IAccountsExcelFileReaderService, AccountsExcelFileReaderService>(new HierarchicalLifetimeManager());
            container.RegisterType<IAccountsTextFileReaderService, AccountsTextFileReaderService>(new HierarchicalLifetimeManager());
            config.DependencyResolver = new UnityResolver(container);

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
