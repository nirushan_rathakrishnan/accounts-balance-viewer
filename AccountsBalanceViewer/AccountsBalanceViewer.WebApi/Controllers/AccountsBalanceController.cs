﻿using AccountsBalanceViewer.Domain.Dto;
using AccountsBalanceViewer.Contracts;
using System;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Collections.Generic;

namespace AccountsBalanceViewer.WebApi.Controllers
{
    public class AccountsBalanceController : ApiController
    {
        private IAccountsBalanceService accountsBalanceService;
        private IAccountsFileReaderFactoryService accountsFileReaderFactoryService;

        //using dependency injection in order to decoupled service layer with controller (in future if we want we can have services layer in a different machine with minimum change)
        public AccountsBalanceController(IAccountsBalanceService accountsBalanceService, 
            IAccountsFileReaderFactoryService accountsFileReaderFactoryService)
        {
            this.accountsBalanceService = accountsBalanceService;
            this.accountsFileReaderFactoryService = accountsFileReaderFactoryService;
        }

        [HttpGet]
        [Authorize]     //Admin and normal user can access
        public async Task<IEnumerable<AccountsBalanceDto>> GetAccountsBalance(int month, int year)
        {
            return await accountsBalanceService.GetAccountsBalanceAsync(month, year);
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]    //only admin can access
        public async Task<IEnumerable<ReportDataDto>> GetReportData(int toMonth, int toYear, int fromMonth, int fromYear)
        {
            return await accountsBalanceService.GetReportDataAsync(toMonth, toYear, fromMonth, fromYear);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]    //only admin can access
        public async Task<IHttpActionResult> UploadAccounts()
        {
            var request = HttpContext.Current.Request;

            //get data from HttpContext request
            var file = request.Files[0];
            var month = Convert.ToInt32(request.Form["month"]);
            var year = Convert.ToInt32(request.Form["year"]);

            if ((file != null) && (file.ContentLength > 0) && !string.IsNullOrEmpty(file.FileName))
            {
                //using strategy pattern to get file reader service (using factory pattern in order to maintain dependency injection)
                var fileReaderService = accountsFileReaderFactoryService.GetFileReaderService(file.FileName);

                var accountsData = fileReaderService.ReadFile(file.InputStream);

                await accountsBalanceService.UpdateAccountsAsync(accountsData, month, year);

                return Ok();
            }

            throw new ArgumentException("Arguments are invalid.");
        }
    }
}
