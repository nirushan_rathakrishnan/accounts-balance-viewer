﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AccountsBalanceViewer.Services;
using AccountsBalanceViewer.Data;
using System.Collections.Generic;
using AccountsBalanceViewer.Domain.Entities;
using System.Data.Entity;
using Moq;
using System.Linq;
using System.Data.Entity.Infrastructure;

namespace AccountsBalanceViewer.Tests
{
    /// <summary>
    /// Basic functionality tests are written.
    /// </summary>
    [TestClass]
    public class TestAccountsBalanceService
    {
        [TestMethod]
        public void GetAccountsBalanceAsync_ShouldReturnAccountsBalance()
        {
            var mockContext = GetMockDbContext();

            var accountsBalanceService = new AccountsBalanceService(mockContext.Object);
            var accountBalances = accountsBalanceService.GetAccountsBalanceAsync(4, 2017).Result;

            Assert.AreEqual(3, accountBalances.Count());
            Assert.IsNotNull(accountBalances.FirstOrDefault(b => b.Balance == 4000));
            Assert.IsNotNull(accountBalances.FirstOrDefault(b => b.Balance == 5000));
            Assert.IsNotNull(accountBalances.FirstOrDefault(b => b.Balance == 6000));
        }

        [TestMethod]
        public void GetReportDataAsync_ShouldReturnReportData()
        {
            var mockContext = GetMockDbContext();

            var accountsBalanceService = new AccountsBalanceService(mockContext.Object);
            var reportData = accountsBalanceService.GetReportDataAsync(4, 2017, 12, 2017).Result;

            Assert.AreEqual(3, reportData.Count());
            Assert.IsNotNull(reportData.FirstOrDefault(r => r.AccountCategoryType.Equals("AAA")));
            Assert.IsNotNull(reportData.FirstOrDefault(r => r.AccountCategoryType.Equals("BBB")));
            Assert.IsNotNull(reportData.FirstOrDefault(r => r.AccountCategoryType.Equals("CCC")));
        }

        [TestMethod]
        public void UpdateAccountsAsync_ShouldUpdateDbContext()
        {
            var mockContext = GetMockDbContext();

            Dictionary<string, decimal> accounts = new Dictionary<string, decimal>();
            accounts.Add("AAA", 800);
            accounts.Add("BBB", 400);
            accounts.Add("CCC", 600);

            var accountsBalanceService = new AccountsBalanceService(mockContext.Object);
            var result = accountsBalanceService.UpdateAccountsAsync(accounts, 8, 2016).Result;

            Assert.AreEqual(true, result);
        }

        private Mock<DataContext> GetMockDbContext()
        {
            var categoryTypeData = new List<AccountCategoryType>
            {
                new AccountCategoryType
                {
                    Type = "AAA",
                    AccountsBalances = new List<AccountsBalance>
                    {
                        new AccountsBalance
                        {
                            Balance = 4000,
                            Date = new System.DateTime(2017, 4, 1)
                        },
                        new AccountsBalance
                        {
                            Balance = 4500,
                            Date = new System.DateTime(2017, 5, 1)
                        }
                    }
                },
                new AccountCategoryType
                {
                    Type = "BBB",
                    AccountsBalances = new List<AccountsBalance>
                    {
                        new AccountsBalance
                        {
                            Balance = 5000,
                            Date = new System.DateTime(2017, 4, 1)
                        },
                        new AccountsBalance
                        {
                            Balance = 5500,
                            Date = new System.DateTime(2017, 5, 1)
                        }
                    }
                },
                new AccountCategoryType
                {
                    Type = "CCC",
                    AccountsBalances = new List<AccountsBalance>
                    {
                        new AccountsBalance
                        {
                            Balance = 6000,
                            Date = new System.DateTime(2017, 4, 1)
                        },
                        new AccountsBalance
                        {
                            Balance = 6500,
                            Date = new System.DateTime(2017, 5, 1)
                        }
                    }
                },
            }.AsQueryable();

            var accountCategoryTypeMockSet = new Mock<DbSet<AccountCategoryType>>();

            accountCategoryTypeMockSet.As<IDbAsyncEnumerable<AccountCategoryType>>()
                .Setup(m => m.GetAsyncEnumerator())
                .Returns(new TestDbAsyncEnumerator<AccountCategoryType>(categoryTypeData.GetEnumerator()));

            accountCategoryTypeMockSet.As<IQueryable<AccountCategoryType>>()
                .Setup(m => m.Provider)
                .Returns(new TestDbAsyncQueryProvider<AccountCategoryType>(categoryTypeData.Provider));

            accountCategoryTypeMockSet.As<IQueryable<AccountCategoryType>>().Setup(m => m.Expression).Returns(categoryTypeData.Expression);
            accountCategoryTypeMockSet.As<IQueryable<AccountCategoryType>>().Setup(m => m.ElementType).Returns(categoryTypeData.ElementType);
            accountCategoryTypeMockSet.As<IQueryable<AccountCategoryType>>().Setup(m => m.GetEnumerator()).Returns(categoryTypeData.GetEnumerator());

            var accountsBalanceData = new List<AccountsBalance>().AsQueryable();

            var accountsBalanceMockSet = new Mock<DbSet<AccountsBalance>>();

            accountsBalanceMockSet.As<IDbAsyncEnumerable<AccountsBalance>>()
                .Setup(m => m.GetAsyncEnumerator())
                .Returns(new TestDbAsyncEnumerator<AccountsBalance>(accountsBalanceData.GetEnumerator()));

            accountsBalanceMockSet.As<IQueryable<AccountsBalance>>()
                .Setup(m => m.Provider)
                .Returns(new TestDbAsyncQueryProvider<AccountsBalance>(accountsBalanceData.Provider));

            accountsBalanceMockSet.As<IQueryable<AccountsBalance>>().Setup(m => m.Expression).Returns(accountsBalanceData.Expression);
            accountsBalanceMockSet.As<IQueryable<AccountsBalance>>().Setup(m => m.ElementType).Returns(accountsBalanceData.ElementType);
            accountsBalanceMockSet.As<IQueryable<AccountsBalance>>().Setup(m => m.GetEnumerator()).Returns(accountsBalanceData.GetEnumerator());

            var mockContext = new Mock<DataContext>();
            mockContext.Setup(m => m.AccountCategoryTypes).Returns(accountCategoryTypeMockSet.Object);
            mockContext.Setup(m => m.AccountsBalances).Returns(accountsBalanceMockSet.Object);

            return mockContext;
        }
    }
}
