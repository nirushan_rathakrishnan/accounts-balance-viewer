Web API Targer Framework - .NET Framework 4.5.2

Used Technologies

asp.net web api 2
EntityFramework 6.1.0
IdentityFramework 2.2.0
Microsoft.Owin 3.1.0
Moq 4.7.99	#mocking database for unit testing
Microsoft.VisualStudio.TestTools.UnitTesting

Angular 
Angular cli
npm 4.5.0
nodejs v6.10.2

bitbucket for source controlling

IDEs
Visual Studio Enterprise 2015
Visual Studio Code

To run unit tests on build in Web API
run test with build => test -> run test after build in Visual Studio 2015

Clone project by executing - "git clone https://nirushan_rathakrishnan@bitbucket.org/nirushan_rathakrishnan/accounts-balance-viewer.git"

Web API setup
open asp.net web api project in visual studio 2015 (PROJECT_CLONED_PATH/accounts-balance-viewer/AccountsBalanceViewer)
rebuild the solution
set database connection string (2 places)
1) web.config in AccountsBalanceViewer.WebApi project
2) app.config in AccountsBalanceViewer.Data project
set AccountsBalanceViewer.Data as startup project
open package manager console
select AccountsBalanceViewer.Data as default project in package manager console
run Update-Database in package manager console
set AccountsBalanceViewer.WebApi as startup project
run it

Angular setup
open angular app in code (PROJECT_CLONED_PATH/accounts-balance-viewer/Web/AccountsBalanceViewer)
set web api url to apiUrl variable in api.service.ts file (PROJECT_CLONED_PATH/accounts-balance-viewer/Web/AccountsBalanceViewer/src/app/shared/services/api.service.ts)
run following commands in terminal
1) npm install
2) ng build
3) ng serve
open the hosted url in a browser (default url is:- http://localhost:4200)

Azure Url - http://accountsbalanceviewerweb.azurewebsites.net/#/login

recommendation
Since I'm using token based authorization, SSL will provide better security.

Note - I have attched sample a account text file and a excel account file with this to test.