import { AppPage } from './app.po';
import { element, by, browser } from 'protractor';
import { protractor } from 'protractor/built/ptor';

describe('accounts-balance-viewer App', () => {
  let page: AppPage;
  const username = 'niru12345@gmail.com';
  const paswword = 'Niru123@';
  beforeEach(() => {
    page = new AppPage();
  });

  it('should register', () => {
    page.navigateTo('register');
    element(by.name('name')).sendKeys(username);
    element(by.name('password')).sendKeys(paswword);
    element(by.name('confirmPassword')).sendKeys(paswword);
    element(by.name('submitBtn')).click();
  });

  it('should login', () => {
    page.navigateTo('');
    element(by.name('name')).sendKeys(username);
    element(by.name('password')).sendKeys(paswword);
    element(by.name('submitBtn')).click();
  });

  it('should view home page', () => {
    page.navigateTo('home');
  });

  it('should view upload accounts page', () => {
    page.navigateTo('uploadAccounts');
  });

  it('should view view straightforward accounts page', () => {
    page.navigateTo('balanceStraightforwardView');
  });

  it('should view view report page', () => {
    page.navigateTo('accountsReport');
  });
});
