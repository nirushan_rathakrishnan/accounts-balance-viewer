import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo(path: string) {
    return browser.get('/#/' + path);
  }

  getParagraphText() {
    return element(by.css('app-root h1')).getText();
  }
}
