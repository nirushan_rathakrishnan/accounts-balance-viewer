import { Component, OnInit } from '@angular/core';
import { RegisterService } from './register.service';
import { RegisterModel } from './register.model';
import { Router } from '@angular/router';
import { MessageHandlerService } from '../../shared/services/message-handler.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerModel: RegisterModel = { email: '', password: '', confirmPassword: '', role: 'Admin' };

  constructor(private registerService: RegisterService, private router: Router,
    private messageHandlerService: MessageHandlerService) { }

  ngOnInit() {}

  registerUser() {
    this.registerService.register(this.registerModel).subscribe(
      result => {
        this.messageHandlerService.announceSuccessMessage('User Registered successfully.');
        this.router.navigate(['login']);
      }
    );
  }

}
