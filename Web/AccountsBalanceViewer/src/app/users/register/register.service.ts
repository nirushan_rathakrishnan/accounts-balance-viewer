import { Injectable } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { RegisterModel } from './register.model';

@Injectable()
export class RegisterService {

    constructor(private apiService: ApiService) {}

    register(registerModel: RegisterModel) {
        return this.apiService.post('Account', 'Register', registerModel);
    }
}
