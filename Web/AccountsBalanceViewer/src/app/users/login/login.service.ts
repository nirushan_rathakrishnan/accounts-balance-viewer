import { Injectable } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { LoginModel } from './login.model';

@Injectable()
export class LoginService {
    constructor(private apiService: ApiService) { }

    login(loginModel: LoginModel) {
        return this.apiService.login(loginModel.email, loginModel.password);
    }
}
