import { Component, OnInit } from '@angular/core';
import { LoginModel } from './login.model';
import { LoginService } from './login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginModel: LoginModel = { email: '', password: '' };

  constructor(private loginService: LoginService, private router: Router) { }

  ngOnInit() {
    if (localStorage.getItem('access_token') && localStorage.getItem('access_token').length > 0) {
      this.router.navigate(['home']);
    }
  }

  login() {
    this.loginService.login(this.loginModel).subscribe(
      result => {
        localStorage.setItem('access_token', result.access_token);
        localStorage.setItem('isAdmin', result.isAdmin);

        this.router.navigate(['home']);
      }
    );
  }
}
