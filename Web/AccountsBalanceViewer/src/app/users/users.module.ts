import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersComponent } from './users.component';
import { UsersRoutingModule } from './users-routing.module';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    UsersRoutingModule,
    RouterModule
  ],
  declarations: [UsersComponent]
})
export class UsersModule { }
