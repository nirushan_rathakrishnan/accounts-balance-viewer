import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BalanceStraightforwardViewComponent } from './balance-straightforward-view.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule
  ],
  declarations: [BalanceStraightforwardViewComponent]
})
export class BalanceStraightforwardViewModule { }
