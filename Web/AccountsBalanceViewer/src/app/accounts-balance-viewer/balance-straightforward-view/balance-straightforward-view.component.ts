import { Component, OnInit } from '@angular/core';
import { AccountsBalanceViewerService } from '../accounts-balance-viewer.service';
import { MonthYearModel, AccountsBalanceModel } from '../accounts-balance-viewer.models';

@Component({
  selector: 'app-balance-straightforward-view',
  templateUrl: './balance-straightforward-view.component.html',
  styleUrls: ['./balance-straightforward-view.component.css']
})
export class BalanceStraightforwardViewComponent implements OnInit {
  model: MonthYearModel = { month: 1, year: 2017 };
  accountsBalances: AccountsBalanceModel[] = [];
  yearsList: number[] = [];
  monthsList: { name: string, value: number }[] = [];
  selectedDate: Date;

  constructor(private accountsBalanceViewerService: AccountsBalanceViewerService) { }

  ngOnInit() {
    // for simplicity year list added here. We can have this year list in web api side as a improvment
    for (let i = 1; i <= 50; i++) {
      this.yearsList.push(i + 2000);
    }

    this.monthsList.push({ name: 'January', value: 1 });
    this.monthsList.push({ name: 'February', value: 2 });
    this.monthsList.push({ name: 'March', value: 3 });
    this.monthsList.push({ name: 'April', value: 4 });
    this.monthsList.push({ name: 'May', value: 5 });
    this.monthsList.push({ name: 'June', value: 6 });
    this.monthsList.push({ name: 'July', value: 7 });
    this.monthsList.push({ name: 'August', value: 8 });
    this.monthsList.push({ name: 'September', value: 9 });
    this.monthsList.push({ name: 'October', value: 10 });
    this.monthsList.push({ name: 'November', value: 11 });
    this.monthsList.push({ name: 'December', value: 12 });

  }

  viewBalance() {
    this.accountsBalanceViewerService.getAccountsBalance(this.model).subscribe(
      result => {
        this.selectedDate = new Date();
        this.selectedDate.setMonth(this.model.month - 1);
        this.selectedDate.setFullYear(this.model.year);
        this.accountsBalances = result;
      }
    );
  }
}
