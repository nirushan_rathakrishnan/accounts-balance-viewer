import { Injectable } from '@angular/core';
import { ApiService } from '../shared/services/api.service';
import { MonthYearModel, AccountsReportModel } from './accounts-balance-viewer.models';

@Injectable()
export class AccountsBalanceViewerService {
    constructor(private apiService: ApiService) { }

    uploadAccounts(formData) {
        return this.apiService.post('AccountsBalance', 'uploadAccounts', formData);
    }

    getAccountsBalance(date: MonthYearModel) {
        return this.apiService.get('AccountsBalance', 'GetAccountsBalance', date);
    }

    getReportData(data: AccountsReportModel) {
        return this.apiService.get('AccountsBalance', 'GetReportData', data);
    }
}
