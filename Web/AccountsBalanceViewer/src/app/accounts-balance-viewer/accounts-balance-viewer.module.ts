import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountsBalanceViewerComponent } from './accounts-balance-viewer.component';
import { RouterModule } from '@angular/router';
import { AccountsBalanceViewerRoutingModule } from './accounts-balance-viewer-routing.module';
import { AccountsBalanceViewerService } from './accounts-balance-viewer.service';

@NgModule({
  imports: [
    CommonModule,
    AccountsBalanceViewerRoutingModule,
    RouterModule
  ],
  declarations: [AccountsBalanceViewerComponent],
  providers: [AccountsBalanceViewerService]
})
export class AccountsBalanceViewerModule { }
