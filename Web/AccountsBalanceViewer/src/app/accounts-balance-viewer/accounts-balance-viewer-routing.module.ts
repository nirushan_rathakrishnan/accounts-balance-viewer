import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { UploadAccountsComponent } from './upload-accounts/upload-accounts.component';
import { UploadAccountsModule } from './upload-accounts/upload-accounts.module';
import { BalanceStraightforwardViewComponent } from './balance-straightforward-view/balance-straightforward-view.component';
import { BalanceStraightforwardViewModule } from './balance-straightforward-view/balance-straightforward-view.module';
import { AccountsReportComponent } from './accounts-report/accounts-report.component';
import { AccountsReportModule } from './accounts-report/accounts-report.module';
import { AccountsBalanceViewerComponent } from './accounts-balance-viewer.component';
import { LoginGuard } from '../guards/login.guards';
import { AdminGuard } from '../guards/admin.guards';

const routes: Routes = [
    {
        path: 'uploadAccounts',
        component: UploadAccountsComponent,
        canActivate: [LoginGuard, AdminGuard]
    },
    {
        path: 'balanceStraightforwardView',
        component: BalanceStraightforwardViewComponent,
        canActivate: [LoginGuard]
    },
    {
        path: 'accountsReport',
        component: AccountsReportComponent,
        canActivate: [LoginGuard, AdminGuard]
    },
    {
        path: 'home',
        component: AccountsBalanceViewerComponent,
        canActivate: [LoginGuard]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        UploadAccountsModule,
        BalanceStraightforwardViewModule,
        AccountsReportModule
    ]
})
export class AccountsBalanceViewerRoutingModule { }
