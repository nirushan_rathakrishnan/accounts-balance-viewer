export class MonthYearModel {
    month: number;
    year: number;
}

export class AccountsBalanceModel {
    accountCategoryType: string;
    balance: number;
}

export class AccountsReportModel {
    fromMonth: number;
    fromYear: number;
    toMonth: number;
    toYear: number;
}

export class AccountsData {
    accountCategoryType: string;
    data: AccountCategoryTypeData[];
}

export class AccountCategoryTypeData {
    month: number;
    year: number;
    balance: number;
}
