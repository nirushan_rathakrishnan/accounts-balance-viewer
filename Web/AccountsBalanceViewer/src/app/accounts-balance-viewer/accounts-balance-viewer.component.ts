import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-accounts-balance-viewer',
  templateUrl: './accounts-balance-viewer.component.html',
  styleUrls: ['./accounts-balance-viewer.component.css']
})
export class AccountsBalanceViewerComponent implements OnInit {
  isAdmin = false;

  constructor(private router: Router) { }

  ngOnInit() {
    this.isAdmin = localStorage.getItem('isAdmin') === 'True';
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['login']);
  }
}
