import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UploadAccountsComponent } from './upload-accounts.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule
  ],
  declarations: [UploadAccountsComponent]
})
export class UploadAccountsModule { }
