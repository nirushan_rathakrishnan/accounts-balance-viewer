import { Component, OnInit } from '@angular/core';
import { AccountsBalanceViewerService } from '../accounts-balance-viewer.service';
import { MonthYearModel } from '../accounts-balance-viewer.models';
import { MessageHandlerService } from '../../shared/services/message-handler.service';

@Component({
  selector: 'app-upload-accounts',
  templateUrl: './upload-accounts.component.html',
  styleUrls: ['./upload-accounts.component.css']
})
export class UploadAccountsComponent implements OnInit {
  model: MonthYearModel = { month: 1, year: 2017 };
  yearsList: number[] = [];
  monthsList: { name: string, value: number }[] = [];
  selectedFile: File;
  isFileSelected = false;

  constructor(private accountsBalanceViewerService: AccountsBalanceViewerService,
    private messageHandlerService: MessageHandlerService) { }

  ngOnInit() {
    // for simplicity year list added here. We can have this year list in web api side as a improvment
    for (let i = 1; i <= 50; i++) {
      this.yearsList.push(i + 2000);
    }

    this.monthsList.push({ name: 'January', value: 1 });
    this.monthsList.push({ name: 'February', value: 2 });
    this.monthsList.push({ name: 'March', value: 3 });
    this.monthsList.push({ name: 'April', value: 4 });
    this.monthsList.push({ name: 'May', value: 5 });
    this.monthsList.push({ name: 'June', value: 6 });
    this.monthsList.push({ name: 'July', value: 7 });
    this.monthsList.push({ name: 'August', value: 8 });
    this.monthsList.push({ name: 'September', value: 9 });
    this.monthsList.push({ name: 'October', value: 10 });
    this.monthsList.push({ name: 'November', value: 11 });
    this.monthsList.push({ name: 'December', value: 12 });
  }

  fileChange(event) {
    this.selectedFile = undefined;
    this.isFileSelected = false;
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      this.selectedFile = fileList[0];
      this.isFileSelected = true;
    }
  }

  uploadAccountsFile() {
    const fileName = this.selectedFile.name;

    // create formData and append data
    const formData: FormData = new FormData();
    formData.append('uploads[]', this.selectedFile, fileName);
    formData.append('month', this.model.month.toString());
    formData.append('year', this.model.year.toString());

    this.accountsBalanceViewerService.uploadAccounts(formData).subscribe(
      result => {
        this.messageHandlerService.announceSuccessMessage('Accounts file uploaded successfully.');
      }
    );
  }
}
