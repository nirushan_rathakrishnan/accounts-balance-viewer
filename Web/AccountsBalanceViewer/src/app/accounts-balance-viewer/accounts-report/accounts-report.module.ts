import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountsReportComponent } from './accounts-report.component';
import { ChartModule } from 'primeng/primeng';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    ChartModule,
    FormsModule,
    RouterModule
  ],
  declarations: [AccountsReportComponent]
})
export class AccountsReportModule { }
