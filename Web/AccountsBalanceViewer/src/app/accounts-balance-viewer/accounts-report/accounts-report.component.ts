import { Component, OnInit } from '@angular/core';
import { AccountsReportModel, AccountsData } from '../accounts-balance-viewer.models';
import { AccountsBalanceViewerService } from '../accounts-balance-viewer.service';

@Component({
  selector: 'app-accounts-report',
  templateUrl: './accounts-report.component.html',
  styleUrls: ['./accounts-report.component.css']
})
export class AccountsReportComponent implements OnInit {
  data: any;
  model: AccountsReportModel = { toMonth: 12, toYear: 2017, fromMonth: 1, fromYear: 2017 };
  lineGraphBorderColors = ['#565656', '#4bc0c0', '#FF5733', '#33A2FF', '#FF33E6'];

  yearsList: number[] = [];
  monthsList: { name: string, value: number }[] = [];

  constructor(private accountsBalanceViewerService: AccountsBalanceViewerService) { }

  ngOnInit() {
    // for simplicity year list added here. We can have this year list in web api side as a improvment
    for (let i = 1; i <= 50; i++) {
      this.yearsList.push(i + 2000);
    }

    this.monthsList.push({ name: 'January', value: 1 });
    this.monthsList.push({ name: 'February', value: 2 });
    this.monthsList.push({ name: 'March', value: 3 });
    this.monthsList.push({ name: 'April', value: 4 });
    this.monthsList.push({ name: 'May', value: 5 });
    this.monthsList.push({ name: 'June', value: 6 });
    this.monthsList.push({ name: 'July', value: 7 });
    this.monthsList.push({ name: 'August', value: 8 });
    this.monthsList.push({ name: 'September', value: 9 });
    this.monthsList.push({ name: 'October', value: 10 });
    this.monthsList.push({ name: 'November', value: 11 });
    this.monthsList.push({ name: 'December', value: 12 });

    // this.data = {
    //   labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    //   datasets: [
    //     {
    //       label: 'First Dataset',
    //       data: [65, 59, 80, 81, 56, 55, 40]
    //     },
    //     {
    //       label: 'Second Dataset',
    //       data: [28, 48, 40, 19, 86, 27, 90]
    //     }
    //   ]
    // };
  }

  viewReport() {
    this.accountsBalanceViewerService.getReportData(this.model).subscribe(
      (accountsData: AccountsData[]) => {
        const datasets: { label: string, data: number[], borderColor: string }[] = [];
        let borderColorIndex = 0;
        for (const account of accountsData) {
          const accountDataset: { label: string, data: number[], borderColor: string } = {
            label: account.accountCategoryType,
            data: [],
            borderColor: this.lineGraphBorderColors[borderColorIndex]
          };
          borderColorIndex++;

          const fromDate = new Date(this.model.fromYear, this.model.fromMonth - 1, 1);
          const toDate = new Date(this.model.toYear, this.model.toMonth - 1, 1);
          while (fromDate.getTime() <= toDate.getTime()) {
            const monthData = account.data.find(d => {
              return d.month === (fromDate.getMonth() + 1) && d.year === fromDate.getFullYear();
            });

            if (monthData) {
              accountDataset.data.push(monthData.balance);
            } else {
              accountDataset.data.push(0);
            }

            fromDate.setMonth(fromDate.getMonth() + 1);
          }

          datasets.push(accountDataset);
        }

        const fromDate = new Date(this.model.fromYear, this.model.fromMonth - 1, 1);
        const toDate = new Date(this.model.toYear, this.model.toMonth - 1, 1);
        const labels: string[] = [];
        while (fromDate.getTime() <= toDate.getTime()) {
          labels.push((fromDate.getMonth() + 1) + '-' + fromDate.getFullYear());
          fromDate.setMonth(fromDate.getMonth() + 1);
        }

        this.data = {
          labels: labels,
          datasets: datasets
        };
      }
    );
  }

}
