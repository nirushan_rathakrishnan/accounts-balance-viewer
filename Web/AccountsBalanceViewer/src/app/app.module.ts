import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { UsersModule } from './users/users.module';
import { ApiService } from './shared/services/api.service';
import { HttpModule } from '@angular/http';
import { MessageHandlerService } from './shared/services/message-handler.service';
import { AccountsBalanceViewerModule } from './accounts-balance-viewer/accounts-balance-viewer.module';
import { LoginGuard } from './guards/login.guards';
import { AdminGuard } from './guards/admin.guards';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    HttpModule,
    BrowserModule,
    RouterModule,
    UsersModule,
    AccountsBalanceViewerModule,
    AppRoutingModule
  ],
  providers: [ApiService, MessageHandlerService, LoginGuard, AdminGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
