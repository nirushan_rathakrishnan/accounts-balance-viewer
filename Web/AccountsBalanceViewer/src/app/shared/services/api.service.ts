import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { MessageHandlerService } from './message-handler.service';
import 'rxjs/add/operator/share';

@Injectable()
export class ApiService {
    private static apiUrl = 'http://localhost:56150';   // set the web api url

    constructor(private http: Http, private errorMessageHandlerService: MessageHandlerService) { }

    login(email, password) {
        const headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });

        const options = new RequestOptions({});
        options.headers = headers;

        const apiCallObservable = this.http.post(ApiService.apiUrl + '/Token',
            'username=' + encodeURIComponent(email) +
            '&password=' + encodeURIComponent(password) +
            '&grant_type=password',
            options)
            .map(response => {
                const result = this.getResponseData(response);
                return result;
            }).share();

        this.handleErrorResponse(apiCallObservable);

        return apiCallObservable;
    }

    post(controller: string, action: string, data: any) {
        const options = new RequestOptions({});
        const headers = new Headers({ 'Authorization': 'Bearer ' + localStorage.getItem('access_token') });
        options.headers = headers;

        const apiCallObservable = this.http.post(ApiService.apiUrl + '/api/' + controller + '/' + action, data, options)
            .map(response => {
                const result = this.getResponseData(response);
                return result;
            }).share();

        this.handleErrorResponse(apiCallObservable);

        return apiCallObservable;
    }

    get(controller: string, action: string, data: any) {
        const options = new RequestOptions({});
        const headers = new Headers({ 'Authorization': 'Bearer ' + localStorage.getItem('access_token') });
        options.headers = headers;

        // Construct parameters.
        const searchParams = new URLSearchParams();
        if (data) {
            for (const property in data) {
                if (data[property]) {
                    searchParams.set(property, encodeURIComponent(data[property]));
                }
            }
        }

        options.search = searchParams;

        const apiCallObservable = this.http.get(ApiService.apiUrl + '/api/' + controller + '/' + action, options)
            .map(response => {
                const result = this.getResponseData(response);
                return result;
            }).share();

        this.handleErrorResponse(apiCallObservable);

        return apiCallObservable;
    }

    getResponseData(response) {
        const bodyLength = response.text().length;
        if (bodyLength === 0) {
            return response;
        }

        return response.json();
    }

    handleErrorResponse(apiCallObservable) {
        apiCallObservable.subscribe(
            data => {

            },
            err => {
                this.errorMessageHandlerService.announceErrorMessage(err._body);
            }
        );
    }
}
